import { Header } from "components";
import { Container } from "components/UI/Container";
import React from "react";

const index = () => {
  return (
    <div>
      <Container>
        <Header />
      </Container>
    </div>
  );
};

export default index;
